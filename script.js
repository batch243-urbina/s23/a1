// console.log(`Hello po`);

const trainer = {
  name: "Ash Ketchum",
  age: 10,
  pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
  friends: { hoenn: ["Max", "May"], kanto: ["Brock", "Misty"] },
  talk: function (index) {
    console.log(`${this.pokemon[index]}! I choose you!`);
  },
};
console.log(trainer);
console.log(`Result of dot notation`);
console.log(trainer.name);
console.log(`Result of square bracket notation`);
console.log(trainer.pokemon);
console.log(`Result of talk method`);
trainer.talk(0);

function Pokemon(name, level) {
  this.pokemonName = name;
  this.pokemonLevel = level;
  this.pokemonHealth = 2 * level;
  this.pokemonAttack = level;

  this.tackle = function (targetPokemon) {
    console.log(`${this.pokemonName} tackles ${targetPokemon.pokemonName}`);
    console.log(
      `${targetPokemon.pokemonName}' health is now reduced to ${
        targetPokemon.pokemonHealth - this.pokemonAttack
      }`
    );

    if (targetPokemon.pokemonHealth - this.pokemonAttack <= 0) {
      this.fainted(targetPokemon.pokemonName);
    }
  };

  this.fainted = function (targetPokemon) {
    console.log(`${targetPokemon} fainted!`);
  };
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

mewtwo.tackle(pikachu);
mewtwo.tackle(geodude);
